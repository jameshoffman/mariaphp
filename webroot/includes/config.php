<?php

return [
    'DEBUG' => true,
    
    //
    // WARNING
    // Make sure you don't commit/share private database credentials
    //
    'DB_HOST' => 'localhost',
    'DB_USER' => 'root',
    'DB_PASSWORD' => 'toor',
    'DB_NAME' => 'mariaphp',
    'DB_PORT' => 3306,

    '/' => '/pages/home', 
    // Default file for url /

    'auth' => '/pages/...',
    // Redirect destination when authentication is required
    // applies to pages ending with .auth.php 	
     
    'already_auth' => '/pages/...',  
    // Redirect destination when already logged in, 
    // applies to pages ending with .auth-redirect.php
];
